""" 
Faces and eyes detectors base class.

Authors: Julia Cohen @ LR Technologies - 2023
Last updated: 10/2023

"""

from abc import ABC, abstractmethod
from pprint import pformat

import cv2


class DetectionData(ABC):
    """
    Abstract base class used to store detection data.
    """
    def __init__(self, 
                 bbox=None,
                 landmarks=None,
                 is_absolute=False,
                 image_size=None,  # W, H
                 is_3d=False):
        """Constructs a DetectionData object using either bbox or landmarks input or both.

        Parameters
        ----------
        bbox : list, optional
            List of 4 values corresponding to [x, y, w, h], by default None
        landmarks : list of list of floats, optional
            List of landmarks coordinates as [x, y, z], by default None
        is_absolute : boolean, optional
            If the bounding box and/or landmarks are absolute or relative values, 
            by default False
        image_size : tuple, optional
            Image size as (width, height), by default None
            Warning: this argument is MANDATORY if is_absolute is True
        is_3d : boolean, optional
            Indicator for 2D or 3D landmarks, by default False
        """
        super().__init__()
        assert (bbox is not None) or (landmarks is not None), \
            "Should be at least one of `bbox` or `landmarks` argument."
        
        if is_absolute:
            assert image_size is not None, \
                "Argument `image_size` must be provided if data is in absolute form."
            bbox = self._bbox_to_rel(bbox, image_size) if bbox is not None else bbox  # (x, y, w, h) with (x,y) the top left corner
            landmarks = self._landmarks_to_rel(landmarks, image_size) if landmarks is not None else landmarks  # list of (x, y, z), z=0 if is_3d is false

        self.bbox = bbox  
        self.landmarks = landmarks 
        self._image_size = image_size
        self._is_3d = is_3d

        self._named_landmarks = None
        self._authorized_names = list()
    
    def __len__(self):
        """Number of elements in the data structure corresponding to one data point.
        It corresponds to 1 for a bounding box, and the number of landmarks for a landmarks data point.

        Returns
        -------
        int
            Length of the data point
        """
        if self.landmarks is not None:
            return len(self.landmarks)
        else:
            return 1

    def get_bbox(self, absolute=False):
        if absolute:
            return self._bbox_to_abs()
        else:
            return self.bbox

    def get_landmarks(self, absolute=False):
        if absolute:
            return self._landmarks_to_abs()
        else:
            return self.landmarks

    def landmarks_names(self):
        if self.landmarks is not None:
            return self._authorized_names
        else:
            return None
    
    @staticmethod
    def _bbox_to_rel(bbox, image_size):
        x = float(bbox[0])/image_size[0]
        y = float(bbox[1])/image_size[1]
        w = float(bbox[2])/image_size[0]
        h = float(bbox[3])/image_size[1]
        return (x, y, w, h)

    def _bbox_to_abs(self): 
        width, height = self._image_size[0], self._image_size[1]
        x = int(self.bbox[0]*width)
        y = int(self.bbox[1]*height)
        w = int(self.bbox[2]*width)
        h = int(self.bbox[3]*height)
        return (x, y, w, h)

    @staticmethod
    def _landmarks_to_rel(landmarks, image_size):
        return [(float(x)/image_size[0], float(y)/image_size[1], float(z)) for (x, y, z) in landmarks]

    def _landmarks_to_abs(self):
        width, height = self._image_size[0], self._image_size[1]
        return [(int(x*width), int(y*height), z) for (x, y, z) in self.landmarks]

    def get_landmark_by_name(self, name):
        if self._named_landmarks is None:
            self._compute_named_landmarks()
        if name not in self._authorized_names:
            print(f"Landmark {name} cannot be computed. "
                  f"Authorized landmarks are: {self._authorized_names}")
            return None
        if name not in self._named_landmarks.keys():
            return self._compute_special(name)  # /!\ return the value, not an index
        return self.landmarks[self._named_landmarks[name]]

    @abstractmethod
    def _compute_named_landmarks(self):
        pass

    @abstractmethod
    def _compute_special(self, name):
        pass

    def __repr__(self) -> str:
        info = {'type': self.__class__,
                'bbox': self.bbox,
                'landmarks': self.landmarks
                }
        return pformat(info, indent=1, compact=True)


class BaseDetector(ABC):
    """Base class for face and landmarks detectors in images.
    
    Warning: This class should not be used directly.
    Use derived classes instead.
    """
    def __init__(self):
        super().__init__()
        self.detector = None
    
    def preprocess(self, frame):
        return frame

    def process(self, frame):
        processed_frame = self.preprocess(frame)
        detections = self.detect(processed_frame)
        return self.postprocess(detections)

    @abstractmethod
    def detect(self, frame):
        pass

    @abstractmethod
    def postprocess(self, detections):
        """Converts detector output into DetectionData
        """
        pass

    def draw(self, frame, detection, bbox=True, landmarks=True):
        """Draw a detection on a frame.
        By default, both the bounding box and landmarks will be drawn on the frame, 
        if they are available.

        In this version, detection can be a single bounding box as a list or a list of landmarks.
        In the future, this will be deprecated and only DetectionData will be accepted.

        Parameters
        ----------
        frame : ndarray
            Image to draw on.
        detection : DetectionData
            Detection data to draw on the frame
        bbox : boolean
            Flag to draw the bounding box, by default True
        landmarks : boolean
            Flag to draw the landmarks, by default True
        """
        if bbox:
            self.draw_bbox(frame, detection)
        if landmarks:
            self.draw_landmarks(frame, detection)
    
    def draw_bbox(self, frame, detection):
        if isinstance(detection, DetectionData):
            xywh = detection.get_bbox()
        else:
            assert isinstance(detection, list) and len(detection) == 4, \
                    f"Error with type of 'detection' argument for bbox, got {detection}"
            print("DEBUG - Got a list of coordinates for bbox!" 
                  "Please create a DetectionData object.")
            xywh = detection
        if xywh is not None:
                self._draw_one_box(frame, xywh)
    
    def draw_landmarks(self, frame, detection):
        if isinstance(detection, DetectionData):
            lmks = detection.get_landmarks()
        else:
            assert isinstance(detection, list) and \
                isinstance(detection[0], list) and \
                    len(detection[0]) == 3, \
                        f"Error with type of 'detection' argument for landmarks, got {detection}"
            print("DEBUG - Got a list of points for landmarks!" 
                  "Please create a DetectionData object.")
            lmks = detection
            
        for lmk in lmks:
            self._draw_one_landmark(frame, lmk)

    def _draw_one_box(self, frame, xywh):
        x, y, w, h = xywh
        height, width, _ = frame.shape
        x = int(x*width)
        y = int(y*height)
        w = int(w*width)
        h = int(h*height)
        cv2.rectangle(frame, (x, y), (x+w, y+h), color=(0, 0, 255))
    
    def _draw_one_landmark(self, frame, lmk):
        x, y, _ = lmk
        height, width, _ = frame.shape
        x = int(x*width)
        y = int(y*height)
        cv2.circle(frame, center=(x, y), radius=1, color=(0, 255, 0))

if __name__ == "__main__":
    print(" **** Testing BaseDetector **** ")
    
    print("This should trigger a TypeError because BaseDetector is an abstract class:")
    try:
        det = BaseDetector()
    except TypeError as err:
        print(f"Got Type error: {err}")
    
    print("\n **** Testing DetectionData **** ")

    print("This should trigger a TypeError because DetectionData is an abstract class:")
    try:
        data = DetectionData()
    except TypeError as err:
        print(f"Got Type error: {err}")
    
    