"""
Face and landmarks detectors using mediapipe library.

Authors: Julia Cohen @ LR Technologies - 2023
Last updated: 10/2023

"""
import warnings

import cv2
import mediapipe as mp
from mediapipe.python.solutions.face_detection import FaceDetection
from mediapipe.python.solutions.face_mesh import FaceMesh, FACEMESH_NUM_LANDMARKS, \
    FACEMESH_NUM_LANDMARKS_WITH_IRISES

from lr_facetracker.detection._base import BaseDetector, DetectionData
from lr_facetracker.detection._facemesh_landmarks import V1, V2
from lr_facetracker.utils import opencv2rgb, middle_point


mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles

class MediapipeData(DetectionData):
    """
    Data object for mediapipe detectors
    """
    def __init__(self, bbox, landmarks, mp_detections=None, is_3d=False):
        """Constructs a MediapipeData object

        Parameters
        ----------
        bbox : list of floats
            Contains [x_min, y_min, width, height] of the box 
            containing the detected face.
        landmarks : list of list of floats
            Contains a list of (x, y, z) positions of keypoints on the face.
            Note: z might be 0 if the detector returns 2D landmarks.
        mp_detections: 
            Detections in the mediapipe format, directly from the detector (for drawing purposes),
            by default None
        is_3d : boolean, optional
            Indicator for 2D or 3D landmarks, by default False
        """
        super().__init__(bbox, landmarks, 
                         image_size=None, 
                         is_3d=is_3d, 
                         is_absolute=False)
        self._mp_detections = mp_detections
        self._compute_named_landmarks()
    
    def _compute_named_landmarks(self): 
        self._named_landmarks = dict()
        if len(self) == 6:
            self._named_landmarks['right_eye'] = 0
            self._named_landmarks['left_eye'] = 1
            self._named_landmarks['nose_tip'] = 2
            self._named_landmarks['mouth_center'] = 3
            self._named_landmarks['right_ear'] = 4
            self._named_landmarks['left_ear'] = 5
            self._authorized_names = list(self._named_landmarks.keys())
            self._authorized_names.append('between_eyes')
        else:
            self._named_landmarks['right_eye_ext'] = 33
            self._named_landmarks['right_eye_int'] = 133
            self._named_landmarks['left_eye_ext'] = 263
            self._named_landmarks['left_eye_int'] = 362
            self._named_landmarks['nose_tip'] = 1
            self._named_landmarks['right_mouth'] = 61
            self._named_landmarks['left_mouth'] = 308
            self._named_landmarks['right_ear'] = 127
            self._named_landmarks['left_ear'] = 356
            self._named_landmarks['chin'] = 152
            self._named_landmarks['forehead'] = 10
            self._authorized_names = list(self._named_landmarks.keys())
            self._authorized_names.append("right_eye")
            self._authorized_names.append("left_eye")
            self._authorized_names.append("mouth_center")
            self._authorized_names.append("mouth")
    
    def _compute_special(self, name):
        if name == "left_eye":
            return middle_point(self.get_landmark_by_name('left_eye_ext'),
                                self.get_landmark_by_name('left_eye_int'))
        elif name == "right_eye":
            return middle_point(self.get_landmark_by_name('right_eye_ext'),
                                self.get_landmark_by_name('right_eye_int'))
        elif name == "mouth":
            return self.get_landmark_by_name('mouth_center')
        elif name == "mouth_center":
            return middle_point(self.get_landmark_by_name('right_mouth'),
                                self.get_landmark_by_name('left_mouth'))
        elif name == "between_eyes":
            return middle_point(self.get_landmark_by_name('left_eye_int'),
                                self.get_landmark_by_name('right_eye_int'))
        else:
            print(f"Unknown landmark name `{name}`")
            return None
    
    def select(self, subset=1):
        """Select one of the face landmarks subsets defined in ._facemesh_landmarks.py

        Parameters
        ----------
        subset : int, optional
            Identifier of the subset, by default 1 
            Possible values: 
                - 0 for all landmarks
                - 1 for subset V1 (13 points)
                - 2 for subset V2 (69 points)

        Returns
        -------
        list of landmarks
            List of the selected landmarks
        """
        if subset == 0:
            # [pt for i, pt in enumerate(self.landmarks) if i in ALL_POINTS]
            return self.landmarks
        elif subset == 1:
            return [pt for i, pt in enumerate(self.landmarks) if i in V1]
        elif subset == 2:
            return [pt for i, pt in enumerate(self.landmarks) if i in V2]
        else:
            warnings.warn(f"Unknown subset {subset}, should be 0, 1 or 2")
        
    def get_mesh(self):
        """Get the original mesh detection result from mediapipe detector.
        Useful for drawing purposes only.

        Returns
        -------
        mediapipe facemesh solution
            Output of a MeshFaceDetector from mediapipe
        """
        return self._mp_detections


class MediapipeDetector(BaseDetector):
    """
    Base class (abstract) for mediapipe face and mesh detectors
    """
    def __init__(self):
        super().__init__()
    
    def preprocess(self, frame):
        return opencv2rgb(frame)

    def detect(self, frame):
        return self.detector.process(frame)
    
    def process(self, frame):
        return super().process(frame)


class FaceDetector(MediapipeDetector):
    """
    Face detector using mediapipe toolkit.
    """
    def __init__(self, min_detection_confidence=0.5, close_to_camera=True):
        """Constructs a FaceDetector object using mediapipe toolkit.

        Parameters
        ----------
        min_detection_confidence : float, optional
            Minimum confidence of the detector to keep a detected face, by default 0.5
        close_to_camera : boolean, optional
            Uses a detector specialized in close-range faces, by default True
        """
        super().__init__()
        model_selection = 0 if close_to_camera else 1
        self.detector = FaceDetection(min_detection_confidence=min_detection_confidence,
                                      model_selection=model_selection)

    def detect(self, frame):
        """
        
        """
        return super().detect(frame)

    def postprocess(self, detections):
        """
        Takes the first detection from the list and converts it to a common format.

        Parameters
        ----------
        detections : _type_
            Output of a mediapipe Face detector.
            Detections are in .detections as a list with relative format.
            Each element has multiple attributes (accessibles with the 
            .attr notation):
                - label_id
                - score
                - location_data : {
                    - format
                    - relative_bounding_box : {
                        - xmin
                        - ymin
                        - width
                        - heigt
                    }
                    - relative_keypoints
                }

        Returns
        -------
        MediapipeData
            Object holding the detected face information.
        """
        if detections.detections is None:
            return None
        first_det = detections.detections[0]
        bbox = [first_det.location_data.relative_bounding_box.xmin,
                first_det.location_data.relative_bounding_box.ymin,
                first_det.location_data.relative_bounding_box.width,
                first_det.location_data.relative_bounding_box.height]
        landmarks = list()
        for kp in first_det.location_data.relative_keypoints:
            landmarks.append((kp.x, kp.y, 0))  # Relative
        data = MediapipeData(bbox=bbox, landmarks=landmarks, is_3d=False)
        return data


class MeshFaceDetector(MediapipeDetector):
    """
    Mesh face detector using the Mediapipe toolkit.
    """
    def __init__(self, single_image=False, 
                 detect_iris=False, 
                 min_detection_confidence=0.5, 
                 min_tracking_confidence=0.5,
                 max_num_faces=1):
        """Constructs a MeshFaceDetector object.

        Parameters
        ----------
        single_image : boolean, optional
            Flag to indicate that the input will be a single image or a video 
            stream, by default False
            Note: corresponds to previous argument `static_image_mode`
        detect_iris : boolean, optional
            Flag to also detect the iris landmarks, by default False
            Note: corresponds to previous argument `refine_landmarks`
        min_detection_confidence : float, optional
            Minimum confidence of the detector to keep a detected face, by default 0.5
        min_tracking_confidence : float, optional
            Minimum confidence of the tracker, by default 0.5
        max_num_faces : int, optional
            Maximum number of faces to detect in each image, by default 1
        """
        super().__init__()

        self.mp_face_mesh = mp.solutions.face_mesh
        self.refine_iris = detect_iris
        self.detector = FaceMesh(static_image_mode=single_image,
                                 max_num_faces=max_num_faces,
                                 refine_landmarks=self.refine_iris,
                                 min_detection_confidence=min_detection_confidence,
                                 min_tracking_confidence=min_tracking_confidence)
        
        self.num_landmarks = FACEMESH_NUM_LANDMARKS if not self.refine_iris else FACEMESH_NUM_LANDMARKS_WITH_IRISES


    def detect(self, frame):
        return super().detect(frame)
    
    def postprocess(self, detections):
        """
        Converts the detected face to a common format.

        Parameters
        ----------
        detections : _type_
            Output of mediapipe FaceMeshDetector.
            Results are in .multi_face_landmarks. 
            For every face in .multi_face_landmarks, there is a list of relative 
            3D landmarks : 
            - landmark[i] : {
                - x
                - y
                - z
            }

        Returns
        -------
        MediapipeData
            Custom data format holding face landmarks information and 
            face bunding box (computed from the landmarks)
        """
        if detections.multi_face_landmarks is None:
            return None
        first_det = detections.multi_face_landmarks[0]
        landmarks = list()
        min_x = 1
        max_x = 0
        min_y = 1
        max_y = 0
        # Landmarks predictions can be outside the image
        for lm in first_det.landmark:
            landmarks.append((lm.x, lm.y, lm.z))
            min_x = lm.x if lm.x < min_x else min_x
            max_x = lm.x if lm.x > max_x else max_x
            min_y = lm.y if lm.y < min_y else min_y
            max_y = lm.y if lm.y > max_y else max_y

        bbox = [min_x, min_y, max_x-min_x, max_y-min_y]
        data = MediapipeData(landmarks=landmarks, 
                             bbox=bbox, 
                             mp_detections=first_det, 
                             is_3d=True)
        return data

    def draw_mesh(self, frame, detection, tesselation=True, contours=True, irises=True):
        landmark_list = detection.get_mesh()
        if tesselation:
            mp_drawing.draw_landmarks(
                image=frame,
                landmark_list=landmark_list,
                connections=self.mp_face_mesh.FACEMESH_TESSELATION,
                landmark_drawing_spec=None,
                connection_drawing_spec=mp_drawing_styles.get_default_face_mesh_tesselation_style()
            )
        if contours:
            mp_drawing.draw_landmarks(
                image=frame,
                landmark_list=landmark_list,
                connections=self.mp_face_mesh.FACEMESH_CONTOURS,
                landmark_drawing_spec=None,
                connection_drawing_spec=mp_drawing_styles.get_default_face_mesh_contours_style()
            )

        if self.refine_iris and irises:
            mp_drawing.draw_landmarks(
                image=frame,
                landmark_list=landmark_list,
                connections=self.mp_face_mesh.FACEMESH_IRISES,
                landmark_drawing_spec=None,
                connection_drawing_spec=mp_drawing_styles.get_default_face_mesh_iris_connections_style()
            )


if __name__ == "__main__":
    print(" **** Testing mediapipe detectors **** ")
    
    print("This should trigger a TypeError because MediapipeDetector is an abstract class.")
    try:
        det = MediapipeDetector()
    except TypeError as err:
        print(f"Got Type error: {err}")

    face_detector = FaceDetector()
    print("Face detector: ", face_detector)

    mesh_detector = MeshFaceDetector()
    print("Mesh Face detector: ", mesh_detector)

    print("No error triggered!")
