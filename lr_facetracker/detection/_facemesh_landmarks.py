"""
Subsets of the mediapipe FaceMeshDetector output landmarks.
"""

LEFT_EYE = [249, 263, 362, 373, 374, 380, 381, 382, 384, 385, 386, 387,
            388, 390, 398, 466]

RIGHT_EYE = [7, 33, 133, 144, 145, 153, 154, 155, 157, 158, 159, 160,
             161, 163, 173, 246]

PUPILS = list(range(468, 478))

MOUTH = [0, 13, 14, 17, 37, 39, 40, 61, 78, 80, 81, 82, 84, 87, 88, 91, 95,
         146, 178, 181, 185, 191, 267, 269, 270, 291, 308, 310, 311, 312,
         314, 317, 318, 321, 324, 375, 402, 405, 409, 415]

MOUTH_CONTOUR = [0, 17, 37, 39, 40, 61, 84, 91, 146, 181, 185, 267, 269, 270,
                 291, 314, 321, 375, 405, 409]

# pt 10 is too high, migh be cut from the frame
FACE_CONTOURS = [9, 152, 127, 356]
LARGE_FACE_CONTOURS = [10, 152, 127, 356]

FULL_EYES = LEFT_EYE+RIGHT_EYE+PUPILS  # total of 42 pts
# eye corners: 362, 263, 33, 133  # mouth corners: 61, 291
V1 = [362, 263, 33, 133, 6, 5, 61, 291, 0] + FACE_CONTOURS  # total of 13pts
V2 = FULL_EYES + [5, 6, 8] + MOUTH_CONTOUR + FACE_CONTOURS  # total of 69pts

ALL_POINTS = list(range(478))