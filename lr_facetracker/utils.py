import math

import cv2
import numpy as np


def opencv2gray(frame):
    return cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)


def opencv2rgb(frame):
    return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)


def rgb2opencv(frame):
    return cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

def middle_point(point1, point2):
    """Compute coordinates of middle point between two points (with any number of coordinates).
    Element-wise average of vectors.

    Args:
        point1 (float): (x, y) or (x, y, z)
        point2 (float): (x, y) or (x, y, z)

    Returns:
        tuple: the middle point as (x, y) or (x, y, z) coordinates
    """
    return np.mean([point1, point2], axis=0)

def distance(point1, point2):
    """Euclidean distance between two points (any dimension)

    Args:
        point1 : iterable
        point2 : iterableof same size as point1

    Returns:
       float : the calculated distance
    """
    return math.dist(point1, point2)


def dot_product(vect1, vect2):
    assert hasattr(vect1, '__iter__'), \
        "First vector in utils.dot_product() is not iterable."
    assert hasattr(vect2, '__iter__'), \
        "Second vector in utils.dot_product() is not iterable."
    return sum((a*b) for a, b in zip(vect1, vect2))


def length_vector(vect):
    return math.sqrt(dot_product(vect, vect))


def angle_between_vectors(vect1, vect2):
    return math.acos(cos_between_vectors(vect1, vect2))


def cos_between_vectors(vect1, vect2):
    """Compute the cosine of the angle between two vectors
    Args:
        vect1, vect2 : norm of the vectors.

    Returns:
        float: cosine of angle
    """
    res = dot_product(vect1, vect2) / \
        (length_vector(vect1)*length_vector(vect2))
    return res


def rad2deg(angle):
    return angle*(180/math.pi)


def deg2rad(angle):
    return angle*math.pi/180


if __name__ == "__main__":
    print("\nTesting middle_point method:")

    assert np.all(middle_point([1., 1., 3.], [3., 3., 1.]) == [2., 2., 2.]), \
        f"Error: got {middle_point([1., 1., 3.], [3., 3., 1.])} instead of {[2., 2., 2.]}"
    
    assert np.all(middle_point([1, 1, 3], [3, 3, 1]) == [2, 2, 2]), \
        f"Error: got {middle_point([1, 1, 3], [3, 3, 1])} instead of {[2, 2, 2]}"
    
    assert np.all(middle_point([1, 2], [2, 1]) == [1.5, 1.5]), \
        f"Error: got {middle_point([1, 2], [2, 1])} instead of {[1.5, 1.5]}"
    
    print("middle_point is okay!")
    