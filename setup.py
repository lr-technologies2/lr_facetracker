from setuptools import setup, find_packages

setup(name='lr_facetracker',
      version='1.0.1',
      description='Face and eyes feature extraction with trackers using a webcam',
      author='LR Technologies - Boris Lenseigne, Julia Cohen, Adrien Dorise, Edouard Villain',
      author_email='{blenseigne, jcohen, adorise, evillain}@lrtechnologies.fr',
      url='https://gitlab.com/lr-technologies2/lr_facetracker',
      py_modules=['lr_facetracker.utils'],
      packages=['lr_facetracker.detection']
     )
