# LR_FaceTracker

This project is a face detector and feature extractor, to be used with standard and learning algorithms.   
It is developped by the R&D team at LR Technologies.   

See `main.py` for usage.   

This project was initiated as [Libergo_EyeTracker](https://gitlab.com/lr-technologies2/libergo-eyetracker).

## Installation

In a virtual environment with Python 3.10:
```bash
pip install -r docs/requirements_linux.txt
```

Then install the package itself:
```bash
pip install .
```

If there is an error in mediapipe installation:   
```bash
sudo apt-get install -y libopencv-contrib-dev
```


## Development roadmap

- [x] :label: V1.0.0 - First release   
FaceMesh detector based on [Mediapipe toolkit](https://github.com/google/mediapipe/tree/master/mediapipe).   

    - [ ] :label: V1.1.0 - More detectors   
Detectors from OpenCV and dlib libraries will be added. 

- [ ] :label: V2.0.0 - Pose estimation methods   
Ad-hoc head pose estimation methods will be added.