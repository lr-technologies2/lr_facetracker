This folder contains outputs, figures, files, stored by default during training.   
They can be deleted during training.
If you want to save them, store them somewhere else after running the application.