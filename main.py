'''
Main file to test the lr_facetracker package: toolbox from LR Technologies to extract face and eyes features from a laptop camera stream.
Last updated by Julia Cohen for LR Technologies - October 2023.
'''

import cv2

from lr_facetracker.detection import FaceDetector, MeshFaceDetector


if __name__ == "__main__":
    print("This is LR Technologies FaceTracker package.")

    face_detector = FaceDetector()
    mesh_detector = MeshFaceDetector(detect_iris=True)

    FLIP_LEFT_RIGHT = True

    video_path = None

    if video_path is None:
        cap = cv2.VideoCapture(0)
    else:
        cap = cv2.VideoCapture(video_path)
    
    assert cap.isOpened(), f"Video Capture object {cap} is not opened."

    success, frame = cap.read()

    while success:
        # process frame
        if FLIP_LEFT_RIGHT:
            frame = cv2.flip(frame, 1)
        
        face_dets = face_detector.process(frame) 
        mesh_dets = mesh_detector.process(frame)

        # draw on frame
        face_frame = frame.copy()
        mesh_frame = frame.copy()
        full_mesh_frame = frame.copy()
        face_detector.draw(face_frame, face_dets)
        mesh_detector.draw(mesh_frame, mesh_dets)
        mesh_detector.draw_mesh(full_mesh_frame, mesh_dets)

        # display
        cv2.imshow("Mediapipe Face Detector", face_frame)
        cv2.imshow("Mediapipe Mesh Face Detector", mesh_frame)
        cv2.imshow("Mediapipe Full Mesh Face Detector", full_mesh_frame)
        key = cv2.waitKey(1)

        if key & 0xFF == ord('q'):
            break

        # read next frame
        success, frame = cap.read()

    cap.release()
    cv2.destroyAllWindows()